import { boot } from 'quasar/wrappers'
import OtpInput from "@bachdgvn/vue-otp-input";
import MyComponent from '../components/MyComponent'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  app.component("v-otp-input", OtpInput);

  app.component('my',MyComponent)
  console.log('otp input',OtpInput);

  // app.use(OtpInput)
})
