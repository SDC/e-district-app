const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),

    children: [
      {
        path: "",
        component: () => import("pages/Index.vue"),
        //  path: '/login', component: () => import('pages/Login.vue')
      },
      { path: "/tracking", component: () => import("pages/Tracking.vue") },
      { path: "/dashboard", component: () => import("pages/Dashboard.vue") },
      { path: "/apply", component: () => import("pages/ApplyCertificate.vue") },
    ],
  },
  { path: "/login", component: () => import("pages/Login.vue") },
  { path: "/otp", component: () => import("pages/OTP.vue") },
  { path: "/signup", component: () => import("pages/Register.vue") },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
